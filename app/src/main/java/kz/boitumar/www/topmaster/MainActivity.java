package kz.boitumar.www.topmaster;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import kz.boitumar.www.topmaster.fragments.FragmentMenu1;

public class MainActivity extends AppCompatActivity {


    @BindView(R.id.menu_tv1) TextView menu_tv1;
    @BindView(R.id.menu_tv2) TextView menu_tv2;
    @BindView(R.id.menu_tv3) TextView menu_tv3;
    @BindView(R.id.menu_tv4) TextView menu_tv4;
    @BindView(R.id.menu_tv5) TextView menu_tv5;

    @BindView(R.id.menu_img1) ImageView menu_img1;
    @BindView(R.id.menu_img2) ImageView menu_img2;
    @BindView(R.id.menu_img3) ImageView menu_img3;
    @BindView(R.id.menu_img4) ImageView menu_img4;
    @BindView(R.id.menu_img5) ImageView menu_img5;

    @BindColor(R.color.yellow)
    int yellow;
    @BindColor(R.color.black)
    int black;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        SetColor();
        menu_tv1.setTextColor(yellow);menu_img1.setColorFilter(yellow);
        replaceFragment(new FragmentMenu1(),"Menu1");

    }

    //  Menu click
    public void MenuClick(View view) {
        SetColor();
        Fragment fragment = null;
        String title="";
        switch (view.getId()) {
            case R.id.menu1:   menu_tv1.setTextColor(yellow);menu_img1.setColorFilter(yellow);
            fragment = new FragmentMenu1(); title = "Menu1";
            break;
            case R.id.menu2:   menu_tv2.setTextColor(yellow);menu_img2.setColorFilter(yellow);
                fragment = new FragmentMenu1(); title = "Menu2";
            break;
            case R.id.menu3:  menu_tv3.setTextColor(yellow);menu_img3.setColorFilter(yellow);
                fragment = new FragmentMenu1(); title = "Menu3";
            break;
            case R.id.menu4:  menu_tv4.setTextColor(yellow);menu_img4.setColorFilter(yellow);
                fragment = new FragmentMenu1(); title = "Menu4";
            break;
            case R.id.menu5:   menu_tv5.setTextColor(yellow);menu_img5.setColorFilter(yellow);
                fragment = new FragmentMenu1(); title = "Menu5";
            break;
        }
        if(fragment!=null){
           replaceFragment(fragment,title);
        }


    }

    public  void replaceFragment(Fragment fragment, String title){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.content_main, fragment, title);
        fragmentTransaction.addToBackStack(title);
        fragmentTransaction.commit();
    }

    private void SetColor() {
        menu_tv1.setTextColor(black);menu_img1.setColorFilter(black);
        menu_tv2.setTextColor(black);menu_img2.setColorFilter(black);
        menu_tv3.setTextColor(black);menu_img3.setColorFilter(black);
        menu_tv4.setTextColor(black);menu_img4.setColorFilter(black);
        menu_tv5.setTextColor(black);menu_img5.setColorFilter(black);
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()>1) {

            String fragmentTag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
            switch (fragmentTag){
                case "Menu1": SetColor();  menu_tv1.setTextColor(yellow);menu_img1.setColorFilter(yellow); break;
                case "Menu2": SetColor();  menu_tv2.setTextColor(yellow);menu_img2.setColorFilter(yellow); break;
                case "Menu3": SetColor();  menu_tv3.setTextColor(yellow);menu_img3.setColorFilter(yellow); break;
                case "Menu4": SetColor();  menu_tv4.setTextColor(yellow);menu_img4.setColorFilter(yellow); break;
                case "Menu5": SetColor();  menu_tv5.setTextColor(yellow);menu_img5.setColorFilter(yellow); break;

            }
            super.onBackPressed();

        }else {
            finish();
        }

    }
}
