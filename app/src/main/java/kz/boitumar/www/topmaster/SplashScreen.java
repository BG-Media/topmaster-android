package kz.boitumar.www.topmaster;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by nurbaqyt on 06.11.17.
 */

public class SplashScreen extends Activity{
    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        handler = new Handler();
        handler.postDelayed(runn1,2000);
    }
    Runnable runn1 = new Runnable() {
        @Override
        public void run() {
           startActivity(new Intent(SplashScreen.this,WelcomeActivity.class));
        }
    };

}
