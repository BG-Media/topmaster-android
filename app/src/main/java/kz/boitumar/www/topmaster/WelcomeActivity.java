package kz.boitumar.www.topmaster;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kz.boitumar.www.topmaster.WelcomeFragments.FragmentWelcome1;
import kz.boitumar.www.topmaster.WelcomeFragments.FragmentWelcome2;
import kz.boitumar.www.topmaster.WelcomeFragments.FragmentWelcome3;

/**
 * Created by nurbaqyt on 06.11.17.
 */

public class WelcomeActivity extends AppCompatActivity {

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @BindView(R.id.dots_layout)
    LinearLayout dots_layout;

    TextView [] dots = new TextView[3];

    ViewPagerAdapter pagerAdapter;
    ArrayList<Fragment> fragments = new ArrayList<Fragment>() ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        fragments.add(new FragmentWelcome1());
        fragments.add(new FragmentWelcome2());
        fragments.add(new FragmentWelcome3());

        init();
        pagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),fragments);
        viewpager.setAdapter(pagerAdapter);
        viewpager.setOnPageChangeListener(viewPagerPageChangeListener);


    }

    private void init() {
        for(int i=0;i<3;i++){
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(40);
            dots[i].setPadding(5,0,5,0);
            dots[i].setTextColor(getResources().getColor(R.color.grayer));
            dots_layout.addView(dots[i]);
        }
        dots[0].setTextColor(getResources().getColor(R.color.gray));
    }

    @OnClick(R.id.next)
    void  next(){
        startActivity(new Intent(WelcomeActivity.this,MainActivity.class));
    }

    //	page change listener
    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            for (int i = 0; i < 3; i++) {
                dots[i].setTextColor(getResources().getColor(R.color.grayer));
            }
            dots[position].setTextColor(getResources().getColor(R.color.gray));
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    public class ViewPagerAdapter extends FragmentPagerAdapter {

         ArrayList<Fragment> fragments;
        public ViewPagerAdapter(FragmentManager fragmentManager, ArrayList<Fragment> fragments) {
            super(fragmentManager);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }

    }

}